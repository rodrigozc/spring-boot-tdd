package tdd.boot.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import tdd.boot.spring.exception.GuitarNotFoundException;
import tdd.boot.spring.model.Guitar;
import tdd.boot.spring.repository.GuitarRepository;

@Service
public class GuitarService {

    @Autowired
    private GuitarRepository guitarRepository;

    public GuitarService(GuitarRepository guitarRepository) {
        this.guitarRepository = guitarRepository;
    }

    @Cacheable("guitars")
    public Guitar getGuitarByModel(String model) throws GuitarNotFoundException {
        Guitar guitar = this.guitarRepository.findByModel(model);
        if (guitar == null) {
            throw new GuitarNotFoundException();
        }
        return guitar;
    }
}
