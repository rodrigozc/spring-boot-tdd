package tdd.boot.spring.repository;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.repository.CrudRepository;
import tdd.boot.spring.model.Guitar;

public interface GuitarRepository extends CrudRepository<Guitar, Long> {

    Guitar findByModel(String model);

}
