package tdd.boot.spring.repository;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import tdd.boot.spring.model.Guitar;

@RunWith(SpringRunner.class)
@DataJpaTest
public class GuitarRepositoryTest {

    @Autowired
    private GuitarRepository guitarRepository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void getGuitarModelShouldReturnDetails() throws Exception {
        //prepare
        Guitar persistedGuitar = this.entityManager.persistFlushFind(new Guitar(null, "Ibanez", "JEM7V", 2018));

        //test
        Guitar guitar = this.guitarRepository.findByModel("JEM7V");

        //assert
        Assertions.assertThat(guitar.getModel()).isEqualTo(persistedGuitar.getModel());
        Assertions.assertThat(guitar.getBrand()).isEqualTo(persistedGuitar.getBrand());

    }

}