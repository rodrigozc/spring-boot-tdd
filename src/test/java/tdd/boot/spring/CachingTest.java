package tdd.boot.spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import tdd.boot.spring.model.Guitar;
import tdd.boot.spring.repository.GuitarRepository;
import tdd.boot.spring.service.GuitarService;

import static org.mockito.BDDMockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@AutoConfigureTestDatabase
public class CachingTest {

    @Autowired
    private GuitarService guitarService;

    @MockBean
    private GuitarRepository guitarRepository;

    @Test
    public void cacheGetGuitarByModel() throws Exception {
        //prepare
        given(guitarRepository.findByModel(anyString())).willReturn(new Guitar(1L, "Ibanez", "JEM7V", 2018));

        //test
        guitarService.getGuitarByModel("JEM7V");
        guitarService.getGuitarByModel("JEM7V");

        //assert
        verify(guitarRepository, times(1)).findByModel("JEM7V");

    }

}
