package tdd.boot.spring.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import tdd.boot.spring.exception.GuitarNotFoundException;
import tdd.boot.spring.model.Guitar;
import tdd.boot.spring.service.GuitarService;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(GuitarController.class)
public class GuitarControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GuitarService guitarService;

    @Test
    public void getGuitarModelShouldReturnDetails() throws Exception {
        given(guitarService.getGuitarByModel(anyString())).willReturn(new Guitar(1L, "Ibanez", "JEM7V", 2008));
        mockMvc.perform(MockMvcRequestBuilders.get("/guitars/JEM7V"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("model").value("JEM7V"))
                .andExpect(jsonPath("brand").value("Ibanez"));
    }

    @Test
    public void getGuitarModelShouldThrowGuitarNotFoundException() throws Exception {
        given(guitarService.getGuitarByModel(anyString())).willThrow(new GuitarNotFoundException());
        mockMvc.perform(MockMvcRequestBuilders.get("/guitars/NOT404"))
                .andExpect(status().isNotFound());
    }

}
