package tdd.boot.spring;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import tdd.boot.spring.model.Guitar;
import tdd.boot.spring.repository.GuitarRepository;
import tdd.boot.spring.service.GuitarService;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private GuitarRepository guitarRepository;

    private Guitar guitar = new Guitar(null, "Ibanez", "JEM7V", 2018);

    @Before
    public void setup() throws Exception {
        this.guitar = this.guitarRepository.save(guitar);
    }

    @After
    public void teardown() throws Exception {
        this.guitarRepository.delete(guitar);
    }

    @Test
    public void getGuitarModelShouldReturnDetails() throws Exception {
        //prepare

        //test
        ResponseEntity<Guitar> response = restTemplate.getForEntity("/guitars/JEM7V", Guitar.class);

        //assert
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getModel()).isEqualTo("JEM7V");
        assertThat(response.getBody().getBrand()).isEqualTo("Ibanez");
        assertThat(response.getBody().getYear()).isEqualTo(2018);
    }

}
