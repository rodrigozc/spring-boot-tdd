package tdd.boot.spring.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import tdd.boot.spring.exception.GuitarNotFoundException;
import tdd.boot.spring.model.Guitar;
import tdd.boot.spring.repository.GuitarRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class GuitarServiceTest {

    @Mock
    private GuitarRepository guitarRepository;

    private GuitarService guitarService;

    @Before
    public void setup() throws Exception {
        this.guitarService = new GuitarService(this.guitarRepository);
    }

    @Test
    public void getGuitarModelShouldReturnDetails() throws Exception {
        //prepare
        given(guitarRepository.findByModel("JEM7V")).willReturn(new Guitar(1L, "Ibanez", "JEM7V", 2018));

        //test
        Guitar guitar = this.guitarService.getGuitarByModel("JEM7V");

        //assert
        assertThat(guitar.getModel()).isEqualTo("JEM7V");
        assertThat(guitar.getBrand()).isEqualTo("Ibanez");

    }

    @Test(expected = GuitarNotFoundException.class)
    public void getGuitarModelShouldThrowGuitarNotFoundException() throws Exception {
        // prepare
        given(guitarRepository.findByModel("JEM7V")).willReturn(null);

        //test
        this.guitarService.getGuitarByModel("JEM7V");
    }


}
